﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Poltax.Models
{
    public class CapitalGains
    {
        public decimal? Amount;
        public double Rate;
        public decimal Tax;
        public string Error;
    }
}
