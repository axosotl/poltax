﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Poltax.Models;

namespace Poltax.Controllers
{
    [Produces("application/json")]
    [Route("api/capitalgains")]
    public class CapitalGainsController : Controller
    {
        // GET api//5
        [HttpGet("{amount}")]
        public CapitalGains Get(decimal amount)
        {
            CapitalGains response = new CapitalGains
            {
                Amount = amount,
                Rate = 0.19
            };

            if (amount < 0)
            {
                response.Error = "What?";
                return response;
            }

            response.Tax = amount * (decimal)response.Rate;
            response.Tax = Decimal.Round(response.Tax, 2);

            if (response.Tax == 0)
            {
                response.Tax = (decimal)0.01;
            }

            return response;
        }
    }
}